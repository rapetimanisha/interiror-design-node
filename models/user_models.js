import { DataTypes, Sequelize } from 'sequelize';
import db from "../config/db.js";

const user_details = db.define("user_table",{
    id:{
        type:DataTypes.INTEGER,
        primaryKey:true,
        autoIncrement:true,
    },
    email:{
        type:DataTypes.STRING
    },
    username:{
        type:DataTypes.STRING
    },
    password:{
        type:DataTypes.STRING
    },
    mobilenumber:{
        type:DataTypes.INTEGER
    },
});

export default user_details;

 