import { DataTypes } from "sequelize";
import db from "../config/db.js";

const contact = db.define("contact_details",{
    id:{
        type:DataTypes.INTEGER,
        autoIncrement:true,
        primaryKey:true
    },
    name: {
        type: DataTypes.STRING,
        
    },
    email: { 
        type: DataTypes.STRING,
        
    },  
    phonenumber:{
        type:DataTypes.BIGINT,
    },
    message:{
        type:DataTypes.STRING,
    },
    role:{
        type:DataTypes.STRING,
    },
})

export default contact; 