import express from "express";

import { created_register_Details,userlogin } from "../controllers/user_controllers.js";
const router = express.Router();

router.post("/created_register_Details", created_register_Details);
router.post("/userlogin", userlogin);

export default router;       