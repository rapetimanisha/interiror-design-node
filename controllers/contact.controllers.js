import {  OPERATION_SUCCESS, OPERATION_FAILED,} from "../functions/response.js";
import contact from "../models/contact.models.js"



export const create_contact_details = async (req, res) => {
    try{
        const {
            name,
            email,
            phonenumber,
            message,
            role
        } = req.body;


        const newcontact ={
            name: name,
            email:email,
            phonenumber:phonenumber,
            message:message,
            role:role|| "phonenumber"
        };
        const createcontactdetails = await contact.create(newcontact);
        console.log("created contact Details:", createcontactdetails);

        if (createcontactdetails) {

            return res.status(200).json({
                status: 'SUCCESS',
                message: 'contact details created successfully',
                result: {
                    contact: createcontactdetails, 
                },
            });
        } else {
            return res.status(400).json(OPERATION_SUCCESS('An error occurred while creating contact details',undefined)
                  
            ); 
        }
    } catch (error) {
      console.error('Caught exception in contact details creation', error);
      return res.status(500).json({
        status: 'FAILURE',
        message: 'An error occurred while creating contact details',
        error: error.message,
      });
    } 
  };
//   4erfbhunkijhhuhu7