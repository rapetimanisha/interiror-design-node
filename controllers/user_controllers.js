import { OPERATION_SUCCESS, OPERATION_FAILED } from "../functions/response.js";
 import user_details from "../models/user_models.js";
 import bcrypt from "bcrypt"; 


export const created_register_Details = async (req, res) => {
  try {
    const { username,email, password, mobilenumber } = req.body;

    const existingUserByemailaddress = await user_details.findOne({
      where: { email: email }
    });

    if (existingUserByemailaddress) {
      return res.status(200).json({ error: "User with this emailaddress already exists" });
    }
    const hashedPassword = await bcrypt.hash(password, 10);
    
    const createdregisterDetails = await user_details.create({
      username:username,
      email: email,
      password: hashedPassword,
      mobilenumber: mobilenumber
    });

    console.log(createdregisterDetails, "createdregisterDetails");

    if (createdregisterDetails) 
    return res.status(200).json(OPERATION_SUCCESS("createdregisterDetails fetched successfully", createdregisterDetails)
 );

} catch (error){
    console.log("An error occured while fetching getqueriesdetails", error);
    return res.status(500).json(OPERATION_FAILED("An error occurred while fetching createdregisterDetails",error)       
);
}
};

export const userlogin = async (req, res) => {
    const { email, password } = req.body;
  
    if (!email || !password) {
        return res.status(400).json({ error: "Required fields missing" });
    }
  
    try {
        const loginuser = await user_details.findOne({
            where: { email: email }
        });
   
        if (!loginuser) {
            return res.status(401).json({ error: "User not found" });
        }
  
        const passwordMatch = await bcrypt.compare(password, loginuser.password);
  
        if (!passwordMatch) {
            return res.status(401).json({ error: "Invalid password" });
        }
  
       
        return res.status(200).json({
            message: "User login successfully",
            status: 200, 
            result: {
                user: loginuser
            },
        }); 
    } catch (error) {
        console.error("Error during user login:", error);
        return res.status(500).json({
            error: "Caught exception during user login",
            details: error.message
        });
    }
  };     