import express from 'express';
import db from './config/db.js';
import cors from 'cors';
const app = express();
app.use(cors());  
app.use(express.json());
app.use(express.urlencoded({
  extended:false
}));
db.authenticate()
  .then(() => {
    console.log("Database connected");
    db.sync();
  }) 
.catch((err) => {
  console.log(err);


  // hgigigggjgigij
});


import user from './routers/user_routers.js'
app.use('/user',user);

// import contact from "./routes/contact.routes.js";
// app.use("/contact", contact)

import contact from "./routers/contact.routes.js";
app.use("/contact",contact)



  const port = process.env.PORT || 2000;
  app.listen(port, () => { 
    console.log(`Server is running on port ${port}`);
  });